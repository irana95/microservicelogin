package com.example.springcdci;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    @GetMapping(value = "/getUser")
    public String getUser() {
        String s="Hello new user!...";
        return s.intern();
    }

}
