FROM openjdk:11
ADD target/springcdci.jar cdci.jar
VOLUME /var/run/docker.sock
EXPOSE 8085
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar", "cdci.jar"]
